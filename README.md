# GeoMojo::Options

Simple C++ header-only options parsing class with example.  

## Description

This is just a simple options parser for my needs. There are other option parsers (e.g. boost options, Argh, etc.) that are much more elaborate. I just need something to capture flags and options with values to strings. I don't attempt to cast the values into non-string types, check boundaries, etc. within this class (which I tend to do in my main program logic anyway). This is just a first cut at this little header file to support another small project I'm working on. I'll probably expand on this project over time. This class parses argc/argh without having the user predefine options as most program option libraries I've use. 

To build/install, do something like this...

````
mkdir -p build
cd build
make package
sudo yum -y install GeoMojo-Options-1.0.0-Linux.rpm
````
Then you should be able to...
````
#include <GeoMojo/Options.h>
````
