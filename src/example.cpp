#include <iostream>
#include "GeoMojo/Options.h"

using namespace std;

int main(int argc, char* argv[])
{
  try 
  {
    // Get user options.
    GeoMojo::Options options(argc, argv);

    options.registerOptionWithoutValue("alpha");
    options.registerOptionWithoutValue("bravo");
    options.registerOptionWithValue("charlie");
    options.registerOptionWithValue("delta");

    options.print();
    /*
    Example output:
    example --alpha --charlie three --echo --foxtrot six
    Registered options without values (i.e. no "flags"):
     * alpha: set
     * bravo: not set
    Registered options with values:
     * charlie: three
     * delta: not set
    Unrecognized arguments:
     * echo
     * foxtrot
    */

    // TODO: provide examples converting string to numeric types

    return EXIT_SUCCESS;
  }
  catch (exception const& ex) 
  {
    cerr << "Error: " << ex.what() << endl;
  }
  catch (...) 
  {
    cerr << "Error: An unexpected exception was raised." << endl;
  }
  return EXIT_FAILURE;
}

