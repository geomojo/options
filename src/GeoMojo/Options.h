#pragma once
#include <regex>
#include <string>
#include <map>
#include <set>
#include <vector>
#include <algorithm>
#include <stdexcept>

using namespace std;

namespace GeoMojo
{
  class Options
  {
  public:
    Options (int &argc, char **argv)
    {
      vector<string> rawArguments;
      for (int i=1; i < argc; ++i)
      {
        string argument(argv[i]);
        // Split strings with equal sign into two tokens
        auto index = argument.find('=');
        if (index != string::npos) 
        {
          rawArguments.push_back(argument.substr(0,index));
          rawArguments.push_back(argument.substr(index+1));
        }
        else
        {
          rawArguments.push_back(string(argument));
        }
      }
      regex shExpr("^-[a-zA-Z]+");
      regex dhExpr("^--[a-zA-Z][a-zA-Z0-9]*");
      map<string,int> keys; // Key is the option name. Value is the number of times set.
      bool nextArgumentCanBeValue(false); // (otherwise, looking for new key)
      string lastKeyName("");
      // A single hyphen can be followed by multiple single-character flags
      // A double hyphen prefixes a single, multicharacter option.
      for(string argument : rawArguments)
      {
        if(regex_match(argument, shExpr)) // Evaluate single-hyphen option(s)
        {
          if(argument.size()==2) // e.g. "-x"
          {
            nextArgumentCanBeValue = true;
          }
          else // e.g. -xyz
          {
            nextArgumentCanBeValue = false;
          }
          for(char& c : argument)
          {
            string key = string(1,c);
            if(key == "-")
            {
              // Skip the hyphen character
              continue;
            }
            
            // Keep track of how many times each option is set
            if(keys.count(key))
            {
              keys[key] = keys[key] + 1;
            }
            else
            {
              keys[key] = 1;
            }
            lastKeyName = key;
          }
        }
        else if(regex_match(argument, dhExpr))  // Evaluate double-hyphen option
        {
          nextArgumentCanBeValue = true;
          string key = argument.substr(2); // Remove the first two characters: "--"
          // Keep track of how many times each option is set
          if(keys.count(key))
          {
            keys[key] = keys[key] + 1;
          }
          else
          {
            keys[key] = 1;
          }
          lastKeyName = key;
        }
        else if (nextArgumentCanBeValue)
        {
          m_capturedOptionsWithValues[lastKeyName] = argument;
          nextArgumentCanBeValue = false;
        }
        else
        {
          throw invalid_argument("Unrecognized option: \"" + argument + "\"");
        }
      }
      for(auto key : keys)
      {
        if(m_capturedOptionsWithValues.count(key.first)==0)
        {
          m_capturedOptionsWithoutValues.emplace(key.first);
        }
      }
    }

    bool isSet(string name)
    {
      if(count(m_capturedOptionsWithoutValues.begin(), m_capturedOptionsWithoutValues.end(), name) || m_capturedOptionsWithValues.count(name)>0)
      {
        return true;
      }
      for(auto capturedOptionWithValue : m_capturedOptionsWithValues)
      {
        if(capturedOptionWithValue.first == name)
        {
          return true;
        }
      }
      return false;
    }

    string getValue(string name)
    {
      if(m_capturedOptionsWithValues.count(name)>0)
      {
        return m_capturedOptionsWithValues[name];
      }
      else if (count(m_capturedOptionsWithoutValues.begin(), m_capturedOptionsWithoutValues.end(), name))
      {
        throw logic_error(name + " is option without value (use isSet(\"" + name + "\") instead, or set option value)");
      }
      else
      {
        throw logic_error(name + " is not set");
      }
    }

    void registerOptionWithoutValue(string option)
    {
      m_registeredOptionsWithoutValues.emplace(option);
    }

    void registerOptionWithValue(string option)
    {
      m_registeredOptionsWithValues.emplace(option);
    }

    set<string> getUnrecognizedArguments()
    {
      set<string> uregisteredArguments;
      for(auto arg : m_capturedOptionsWithoutValues)
      {
        if(!m_registeredOptionsWithoutValues.count(arg))
          uregisteredArguments.emplace(arg);
      }
      for(auto arg : m_capturedOptionsWithValues)
      {
        if(!m_registeredOptionsWithValues.count(arg.first))
          uregisteredArguments.emplace(arg.first);
      }
      return uregisteredArguments;
    }

    void print()
    {
      if(m_registeredOptionsWithoutValues.size() > 0)
      {
        cout << "Registered options without values (i.e. no \"flags\"):" << endl;
        for(auto registeredOptionWithoutValue : m_registeredOptionsWithoutValues)
        {
          cout << " * " << registeredOptionWithoutValue << ": ";
          if(isSet(registeredOptionWithoutValue))
          {
            cout << "set" << endl;
          }
          else
          {
            cout << "not set" << endl;
          }

        }
      }
      else
      {
        cout << "No registered options without values (i.e. no \"flags\")" << endl;
      }
      if(m_registeredOptionsWithValues.size() > 0)
      {
        cout << "Registered options with values:" << endl;
        for(auto registeredOptionWithValue : m_registeredOptionsWithValues)
        {
          cout << " * " << registeredOptionWithValue << ": ";
          if(isSet(registeredOptionWithValue))
          {
            if(m_capturedOptionsWithValues[registeredOptionWithValue].size()>0)
            {
              cout << m_capturedOptionsWithValues[registeredOptionWithValue] << endl;
            }
            else
            {
              cout << "not set" << endl;
            }
          }
          else
          {
            cout << "not set" << endl;
          }

        }
      }
      else
      {
        cout << "No registered options with values" << endl;
      }
      if(getUnrecognizedArguments().size() > 0)
      {
        cout << "Unrecognized arguments:" << endl;
        for(auto option : getUnrecognizedArguments())
        {
          cout << " * " << option;
          if(m_registeredOptionsWithValues.count(option))
          {
            cout << " (expected option with value)";
          }
          cout << endl;
        }
      }
    }

  private:
    // Registered by program
    set<string>         m_registeredOptionsWithoutValues; // AKA "flag" or "switch"
    set<string>         m_registeredOptionsWithValues;

    // Set by user
    set<string>         m_capturedOptionsWithoutValues;
    map<string, string> m_capturedOptionsWithValues;
    set<string> m_unrecognizedArguments;
  };
}



